# py-dungeon-crawler

Using py-game to further learn python in a fun way.

## Windows

### Install python

Download the latest python and make sure you add it to your environment variables. You can find the latest installation here: https://www.python.org/downloads/ 
This installer will have the option to add to your environment variables, check this option.

### Install pygame

To install pygame, run this command in administrator mode:
```
py -m pip install -U pygame --user
```

To verify you've installed it correctly, run this:

```
py -m pygame.examples.aliens
```

### Ubuntu

install pip
```
apt-get -y install python3-pip
```

install pygame
```
python3 -m pip install -U pygame --user
```

test
```
python3 -m pygame.examples.aliens
```

## Running this project

To run the project, navigate to the root directory and run the following command:
```
py -m run-game
```