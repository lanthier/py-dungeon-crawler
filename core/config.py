import os

basePath = os.path.dirname(__file__)
assetsPath = os.path.join(basePath, 'assets')

def getAssetPath(filename):
    return os.path.join(assetsPath, filename)