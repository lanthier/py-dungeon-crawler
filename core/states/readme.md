## Contributing Guide

Contains all of the states of the game. Things like menus, splash screens, gemplay screens,
all will be here at some point.

A state, at this time, also handles interactions between events and actors. When an event happens,
the state is what decides how an actor/component responds.

To create a new state, inherit from the _State object. The _State object will get passed events
from the generic state machine.