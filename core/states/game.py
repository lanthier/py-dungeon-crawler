"""
An example state, this one in particular represents a
gameplay state that contains a rectangle player
"""
from .. import state_machine
from ..components.sprites.actor import Actor
from ..renderer import clear
import pygame as pg

class Game(state_machine._State):
    def __init__(self):
        super().__init__()
        self.rectX = 10
        self.rectY = 10
        self.actor = Actor(300, 300, 'wizard')

    def get_event(self, event):
        pressed = pg.key.get_pressed()
    
    def draw(self):
        clear()
        self.actor.draw()

    def update(self, keys):
        self.actor.update(keys)