"""
Responsible for any actual drawing done
"""

import pygame as pg

_screen = pg.display.set_mode((1200, 700)) #TODO not here pls

def drawRect(x_position, y_position, width, height, draw_color = (255, 255, 255) ):
    pg.draw.rect(_screen, draw_color, pg.Rect(x_position, y_position, width, height))
    pg.display.flip()
    pg.display.update()

def drawSprite(image, rectangle):
    outlineColor = (255, 255, 255)
    #pg.draw.rect(_screen, outlineColor, (rectangle[0] - 1, rectangle[1] + 1, rectangle[2] + 1, rectangle[3] + 1))
    _screen.blit(image, rectangle)
    pg.display.flip()
    pg.display.update()

def clear():
    _screen.fill((0, 0, 0))