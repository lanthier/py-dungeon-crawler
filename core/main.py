from .states import game
from . import game_control
import pygame as pg
def main():
    app = game_control.GameControl()
    state_dict = { "GAME": game.Game() }
    app.state_machine.initialize_states(state_dict, "GAME")
    pg.init()
    app.main()