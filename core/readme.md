## Contributing guide

The core directory will hold all game behavior. This includes drawing, game loops, actor behaviors,
state management, events etc.