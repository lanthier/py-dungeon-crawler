"""
This module creates a generic state machine
"""

class StateManager(object):
    def __init__(self):
        self.done = False
        self.state_dict = { }
        self.state_name = None
        self.current_state = None
        
    def initialize_states(self, state_dict, start_state):
        self.state_dict = state_dict
        self.state_name = start_state
        self.current_state = self.state_dict[self.state_name]

    def update(self, keys):
        if self.current_state.quit:
            self.done = True
        self.current_state.update(keys)
    
    def get_event(self, event):
        self.current_state.get_event(event)

    def draw(self):
        self.current_state.draw()
    
class _State(object):
    """
    Any state for this project should inherit from this class
    """
    def __init__(self):
        self.done = False
        self.quit = False
        self.next = None
        self.previous = None

    def get_event(self, event):
        pass
    
    def update(self, keys):
        pass

    def draw(self):
        pass