import pygame as pg
from ...config import getAssetPath
from ... import renderer

class Actor(object):
    def __init__(self, x_position, y_position, actor_type):
        self.x_position = x_position
        self.y_position = y_position
        self.actor_type = actor_type
        self.current_frame = None
        self.cycle_index = 0
        self.direction = 'left'
        self.state = 'idle'

    def draw(self):
        if(self.current_frame is not None):
            renderer.drawSprite(self.current_frame, (self.x_position, self.y_position, 16, 16))

    def update(self, keys):
        if self.state == 'attack' or self.state == 'magic':
            pass
        elif keys[pg.K_c]:
            self.state = 'magic'
            self.cycle_index = 0
        elif keys[pg.K_SPACE]:
            self.state = 'attack'
            self.cycle_index = 0
        elif keys[pg.K_UP]:
            self.y_position -= 5
            self.state = 'walk'
        elif keys[pg.K_DOWN]:
            self.y_position += 5
            self.state = 'walk'
        elif keys[pg.K_LEFT]:
            self.x_position -= 5
            self.direction = 'left'
            self.state = 'walk'
        elif keys[pg.K_RIGHT]:
            self.x_position += 5
            self.direction = 'right'
            self.state = 'walk'
        else:
            self.state = 'idle'
        self.cycle()

    def cycle(self):
        if(self.cycle_index == 2): 
            self.cycle_index = 0
            if self.state == 'attack' or self.state == 'magic':
                self.state = 'idle'
        else: 
            self.cycle_index += 1

        self.update_frame()

    def update_frame(self):
        self.current_frame = self.get_image(self.calculate_path(self.state, self.direction, self.cycle_index))

    def calculate_path(self, actor_action, actor_direction, cycle_index):
        base_path = '_'.join([actor_action, actor_direction, str(cycle_index)])
        return self.actor_type + '/' + base_path + '.png'
        
    def get_image(self, file_path):
        return pg.image.load(getAssetPath(file_path))