## Contributing Guide

This directory will be for actors/components, can range from npcs and players to inanimate objects like map tiles.

A component is primarily responsible for deciding what it handling what it looks like, where it is, and any actions
this component might take.