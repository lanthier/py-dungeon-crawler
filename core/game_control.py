import pygame as pg
from . import state_machine
from .renderer import clear

class GameControl:
    """
    Control class for entire project, passes events to the statemachine to handle
    and contains the game loop. FPS should be handled here
    """
    def __init__(self):
        self.screen = pg.display.get_surface()
        self.done = False
        self.clock = pg.time.Clock()
        self.fps = 15.0
        self.state_machine = state_machine.StateManager()
        self.keys = None

    def event_loop(self):
        self.keys = pg.key.get_pressed()

        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.done = True
            self.state_machine.get_event(event)

    def draw(self):
        self.state_machine.draw()

    def main(self):
        while not self.done:
            self.event_loop()
            self.clock.tick(self.fps)
            self.update()
            self.draw()
            clear()

    def update(self):
        self.state_machine.update(self.keys)
