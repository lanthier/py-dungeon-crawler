"""
The game entry point
"""

import sys
import pygame as pg

from core.main import main

if __name__ == '__main__':
    main()
    pg.quit()
    sys.exit()
