# Contributing

## Adding features and functionality

### 1. Fork the project
This will give you ownership of your own version of this project.

### 2. Request a branch from the owner
By convention, the branch name should be feat/{branch-name-here}

### 3. Add commits to your fork
Use the git conventions below to add commits

### 4. Submit a merge request into that branch
Will require owner approval. Once the owner approves merge request, you can 
merge it. Make sure to check the remove branch option if it is not a WIP

## Git guidelines

### Commit messages

Adding a commit message should summarize your code. Ff it cannot be 
summarized in one message then you should break your commits up into smaller 
commits.

Descriptions are optional but can be used to describe why you are making the 
code change

Try to use git prefixes in your commit messages:

| Prefix | Description
--- | ---
feat: | A new feature
fix: | A bug fix
docs: | Documentation changes
style: | Changes that don't affect functionality (i.e. missing semicolon)
refactor: | A code change that isn't feat or fix (i.e. abstractions)
perf: | A code change that improves performance
test: | Adding a test
chore: | Changes to build process or other tools